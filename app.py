from flask import Flask
from flask import request

app = Flask(__name__)

class Test:
    def test_add(self,x,y):
        return x+y
    def test_sub(self,x,y):
        return x-y

@app.route('/')
def index():
    return 'Hello Bob from 송승훈' 

@app.route('/add')
def add():
    a = request.args.get('a',type=int)
    b = request.args.get('b',type=int)
    return str(a+b)

@app.route('/sub')
def sub():
    a = request.args.get('a',type=int)
    b = request.args.get('b',type=int)
    return str(a-b)
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8089)